package printer;

public class WelcomePrinter {
    public static final String WELCOME_MESSAGE = "Witaj w katalogu biblioteki.\n" +
            "Aby wykonać komendę wciśnij odpowiadającą jej cyfrę, a następnie enter.";

    public void printWelcomeMessage() {
        System.out.println(WELCOME_MESSAGE);
    }
}
