package printer;

import book.Book;
import book.service.BookListsService;
import printer.bookPrinter.BasicBookPrinter;

import java.util.List;
import java.util.Map;

public class CatalogueInfoPrinter {

    private final BookListsService bookListsService = new BookListsService();
    private final BasicBookPrinter basicBookPrinter = new BasicBookPrinter();

    private final String EMPTY_CATALOGUE_MESSAGE = "Katalog jest pusty";
    private final String NO_RESULTS_MESSAGE = "Nie ma książek spełniających kryteria wyszukiwania.";

    public void printAllBooksFromCatalogue() {
        List<Book> bookList = bookListsService.getTheListOfAllBooks();
        if (bookList.isEmpty()) {
            System.out.println(EMPTY_CATALOGUE_MESSAGE);
            return;
        }
        bookList.forEach(b -> basicBookPrinter.printBookInfo(b));
    }

    public void printBooksNotRentedInXWeeks(Integer numberOfWeeks) {
        List<Book> bookList = bookListsService.getBooksNotRentedInXWeeks(numberOfWeeks);
        if (bookList.isEmpty()) {
            System.out.println(NO_RESULTS_MESSAGE);
            return;
        }
        bookList.forEach(b -> basicBookPrinter.printBookInfo(b));
    }

    public void printPeopleWithRentedBooks() {
        Map<String, Integer> people = bookListsService.getPeopleWithRentedBooks();
        if (people.isEmpty()) {
            System.out.println(NO_RESULTS_MESSAGE);
            return;
        }
        people.forEach((person, numberOfBooks) -> System.out.println(person + ": " + numberOfBooks));
    }


}


