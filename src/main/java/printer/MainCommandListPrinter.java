package printer;

public class MainCommandListPrinter {
    private final String COMMAND_LIST = "1. Dodaj książkę do katalogu.\n" +
            "2. Usuń książkę z katalogu.\n" +
            "3. Wyszukaj książkę.\n" +
            "4. Wyszukaj książki, które nie były wypożyczone w ostatnim okresie.\n" +
            "5. Wypożycz książkę.\n" +
            "6. Wyświetl listę osób, które mają wypożyczone książki\n" +
            "7. Wyświetl cały katalog.\n" +
            "Q. Wyjdź z programu.";

    public void printListOfCommands() {
        System.out.println();
        System.out.println(COMMAND_LIST);
    }
}
