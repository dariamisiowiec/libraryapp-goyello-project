package printer.bookPrinter;

import book.Book;

import java.util.List;

public class BookPrinterFromAuthor extends BasicBookPrinter implements BookPrinter {

    public void print(String arg) {
        List<Book> bookList = bookFindByService.findByAuthor(arg);
        if(bookList.isEmpty()){
            System.out.println(NO_RESULTS_MESSAGE);
            return;
        }
        bookList.forEach(b -> printBookInfo(b));
    }
}
