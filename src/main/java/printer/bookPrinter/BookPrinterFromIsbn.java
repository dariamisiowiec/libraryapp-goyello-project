package printer.bookPrinter;

import book.Book;

public class BookPrinterFromIsbn extends BasicBookPrinter implements BookPrinter {

    public void print(String arg) {
        Book book = bookFindByService.findByIsbn(arg).orElseThrow( () -> new RuntimeException(NO_RESULTS_MESSAGE));
        printBookInfo(book);
    }
}
