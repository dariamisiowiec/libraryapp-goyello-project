package printer.bookPrinter;

import book.Book;

public class BookPrinterFromTitle extends BasicBookPrinter implements BookPrinter {

    public void print(String arg) {
        Book book = bookFindByService.findByTitle(arg).orElseThrow(() -> new RuntimeException(NO_RESULTS_MESSAGE));
        printBookInfo(book);
    }
}
