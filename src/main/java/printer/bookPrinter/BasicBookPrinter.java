package printer.bookPrinter;

import book.Book;
import book.service.BookFindByService;

public class BasicBookPrinter {

    protected final BookFindByService bookFindByService = new BookFindByService();
    protected final String BOOK_INFO = "%s, %s, %s, wypożyczona przez: %s, dnia: %s";
    protected final String NO_RESULTS_MESSAGE = "Nie ma książek spełniających kryteria wyszukiwania.";

    public void printBookInfo(Book book) {
        System.out.println(String.format
                (BOOK_INFO, book.getAuthor(), book.getTitle(), book.getIsbn(), book.getLastRentalPerson(), book.getLastRentalDate()));
    }

}
