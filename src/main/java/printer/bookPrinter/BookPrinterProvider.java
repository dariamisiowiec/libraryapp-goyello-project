package printer.bookPrinter;

public class BookPrinterProvider {

    private final BookPrinter bookPrinterFromTitle = new BookPrinterFromTitle();
    private final BookPrinter bookPrinterFromAuthor = new BookPrinterFromAuthor();
    private final BookPrinter bookPrinterFromIsbn = new BookPrinterFromIsbn();

    private final String ERROR_MESSAGE = "Nieznana komenda";


    public BookPrinter getPrinter(String printerNumber) {
        switch (printerNumber) {
            case "1":
                return bookPrinterFromTitle;
            case "2":
                return bookPrinterFromAuthor;
            case "3":
                return bookPrinterFromIsbn;
            default:
                throw new RuntimeException(ERROR_MESSAGE);
        }
    }

}
