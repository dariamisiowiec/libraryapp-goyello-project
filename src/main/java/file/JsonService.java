package file;

import book.Book;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

public class JsonService {

    public String makeJsonFromList(List<Book> bookList){
        JSONArray jsonArray = new JSONArray();
        for (Book b : bookList) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("title", b.getTitle());
            jsonObject.put("author", b.getAuthor());
            jsonObject.put("isbn", b.getIsbn());
            jsonObject.put("lastRentalDate", b.getLastRentalDate());
            jsonObject.put("lastRentalPerson", b.getLastRentalPerson());
            jsonArray.put(jsonObject);
        }
        return jsonArray.toString();
    }


}
