package file;

import book.Book;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FileService {

    private static FileService instance;
    public static FileService getInstance(){
        if(instance == null){
            instance = new FileService();
        }
        return instance;
    }

    private FileService(){
    }

    private String FILENAME;

    public FileService setFILENAME(String FILENAME) {
        this.FILENAME = FILENAME;
        return this;
    }

    private final JsonService jsonService = new JsonService();

    public List<Book> getListOfBooksFromFile(){
        ObjectMapper mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
        try {
            return mapper.readValue(new File(FILENAME), new TypeReference<ArrayList<Book>>(){});

        }catch (IOException i){
            throw new RuntimeException("Katalog jest pusty");
        }
    }

    public void updateFile(List<Book> newBookList){
     String json = jsonService.makeJsonFromList(newBookList);
        try {
            FileUtils.copyInputStreamToFile(IOUtils.
                    toInputStream(json, "UTF-8"), new File(FILENAME));
        }catch (IOException i){
            throw new RuntimeException("Zapisanie do pliku nie powiodło się ");
        }
    }
}
