package command;

import command.executor.*;

public class CommandExecutorProvider {

    private final CommandExecutor addBookCommandExecutor = new AddBookCommandExecutor();
    private final CommandExecutor deleteBookCommandExecutor = new DeleteBookCommandExecutor();
    private final CommandExecutor searchForBookCommandExecutor = new SearchForBookCommandExecutor();
    private final CommandExecutor getNotRentedBooksCommandExecutor = new GetNotRentedBooksCommandExecutor();
    private final CommandExecutor rentBookCommandExecutor = new RentBookCommandExecutor();
    private final CommandExecutor getPeopleListCommandExecutor = new GetPeopleListCommandExecutor();
    private final CommandExecutor listAllBooksCommandExecutor = new ListAllBooksCommandExecutor();
    private final CommandExecutor exitCommandExecutor = new ExitCommandExecutor();

    private final String ERROR_MESSAGE = "Nie ma takiej komendy.";

    public CommandExecutor getCommand(String commandNumber) {
        switch (commandNumber) {
            case "1":
                return addBookCommandExecutor;
            case "2":
                return deleteBookCommandExecutor;
            case "3":
                return searchForBookCommandExecutor;
            case "4":
                return getNotRentedBooksCommandExecutor;
            case "5":
                return rentBookCommandExecutor;
            case "6":
                return getPeopleListCommandExecutor;
            case "7":
                return listAllBooksCommandExecutor;
            case "Q":
            case "q":
                return exitCommandExecutor;
            default:
                throw new RuntimeException(ERROR_MESSAGE);
        }

    }
}
