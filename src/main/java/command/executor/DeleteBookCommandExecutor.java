package command.executor;

import book.service.BookService;
import input.UserInputReader;
import printer.bookPrinter.BookPrinterFromIsbn;

public class DeleteBookCommandExecutor implements CommandExecutor {

    private final BookService bookService = new BookService();
    private final BookPrinterFromIsbn bookPrinterFromIsbn = new BookPrinterFromIsbn();
    private final UserInputReader userInputReader = new UserInputReader();

    public void execute() {
        System.out.println("Podaj numer ISBN");
        String isbn = userInputReader.getUserInput();
        System.out.println("Próbujesz usunąć książkę: ");
        bookPrinterFromIsbn.print(isbn);
        System.out.println("Czy chcesz kontynuować? tak/nie");
        if (!userInputReader.getUserInput().equals("tak")) {
            System.out.println("Powrót do menu.");
            return;
        }
        bookService.deleteBookFromCatalogue(isbn);
        System.out.println("Usunięto.");
    }
}
