package command.executor;

import printer.CatalogueInfoPrinter;

public class GetPeopleListCommandExecutor implements CommandExecutor {

    private final CatalogueInfoPrinter catalogueInfoPrinter = new CatalogueInfoPrinter();

    public void execute(){
        catalogueInfoPrinter.printPeopleWithRentedBooks();
    }

}
