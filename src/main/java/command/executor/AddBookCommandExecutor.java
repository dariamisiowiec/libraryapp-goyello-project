package command.executor;

import book.service.BookService;
import input.UserInputReader;
import printer.bookPrinter.BookPrinterFromIsbn;

public class AddBookCommandExecutor implements CommandExecutor {

    private final BookService bookService = new BookService();
    private final UserInputReader userInputReader = new UserInputReader();
    private final BookPrinterFromIsbn bookPrinterFromIsbn = new BookPrinterFromIsbn();

    public void execute() {

        System.out.println("Podaj tytuł");
        String title = userInputReader.getUserInput();

        System.out.println("Podaj autora");
        String author = userInputReader.getUserInput();

        System.out.println("Podaj numer ISBN");
        String isbn = userInputReader.getUserInput();

        bookService.addNewBookToCatalogue(title, author, isbn);
        System.out.println("Dodano książkę: ");
        bookPrinterFromIsbn.print(isbn);
    }
}
