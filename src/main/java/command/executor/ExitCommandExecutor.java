package command.executor;

public class ExitCommandExecutor implements CommandExecutor {

    public void execute() {
        System.exit(0);
    }
}
