package command.executor;

import input.UserInputReader;
import printer.bookPrinter.BookPrinterProvider;

public class SearchForBookCommandExecutor implements CommandExecutor {

    private final UserInputReader userInputReader = new UserInputReader();
    private final BookPrinterProvider bookPrinterProvider = new BookPrinterProvider();

    private final String OPTIONS = "1. Po tytule\n" +
            "2. Po autorze\n" +
            "3. Po numerze ISBN";

    public void execute() {
        System.out.println(OPTIONS);
        String printerNumber = userInputReader.getUserInput();
        if (printerNumber.equals("1")) {
            System.out.println("Podaj tytuł");
        } else if (printerNumber.equals("2")) {
            System.out.println("Podaj autora");
        } else if (printerNumber.equals("3")) {
            System.out.println("Podaj numer ISBN");
        } else {
            throw new RuntimeException("Nieznana komenda");
        }
        String input = userInputReader.getUserInput();
        bookPrinterProvider.getPrinter(printerNumber).print(input);
    }
}
