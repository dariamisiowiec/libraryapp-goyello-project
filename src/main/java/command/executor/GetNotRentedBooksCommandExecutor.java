package command.executor;

import input.UserInputParser;
import input.UserInputReader;
import printer.CatalogueInfoPrinter;

public class GetNotRentedBooksCommandExecutor implements CommandExecutor {

    private final CatalogueInfoPrinter bookInfoPrinter = new CatalogueInfoPrinter();
    private final UserInputReader userInputReader = new UserInputReader();
    private final UserInputParser userInputParser = new UserInputParser();

    private final String USER_PROMPT = "Z ilu ostatnich tygodni?";

    public void execute(){
        System.out.println(USER_PROMPT);
        bookInfoPrinter.printBooksNotRentedInXWeeks(userInputParser.getInteger(userInputReader.getUserInput()));
    }
}
