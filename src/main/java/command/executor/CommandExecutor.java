package command.executor;

public interface CommandExecutor {

    void execute();

}
