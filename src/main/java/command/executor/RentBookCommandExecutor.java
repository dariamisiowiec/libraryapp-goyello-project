package command.executor;

import book.service.BookService;
import input.UserInputReader;

public class RentBookCommandExecutor implements CommandExecutor {

    private final UserInputReader userInputReader = new UserInputReader();
    private final BookService bookService = new BookService();

    public void execute() {
        System.out.println("Podaj numer ISBN");
        String isbn = userInputReader.getUserInput();
        System.out.println("Podaj imię i nazwisko wypożyczającego");
        String person = userInputReader.getUserInput();
        bookService.rentBook(isbn, person);
        System.out.println("Wypożyczono.");
    }
}
