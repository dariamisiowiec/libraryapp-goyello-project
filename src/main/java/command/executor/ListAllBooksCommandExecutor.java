package command.executor;

import printer.CatalogueInfoPrinter;

public class ListAllBooksCommandExecutor implements CommandExecutor{

    private final CatalogueInfoPrinter catalogueInfoPrinter = new CatalogueInfoPrinter();

    public void execute() {
        catalogueInfoPrinter.printAllBooksFromCatalogue();
    }
}
