package book;

import java.time.LocalDate;

public class BookBilder {

    private String title;
    private String author;
    private String isbn;
    private LocalDate lastRentalDate;
    private String lastRentalPerson;

    public BookBilder withTitle(String title) {
        this.title = title;
        return this;
    }

    public BookBilder withAuthor(String author) {
        this.author = author;
        return this;
    }

    public BookBilder withISBN(String isbn) {
        this.isbn = isbn;
        return this;
    }

    public BookBilder withNoLastRentalDate(){
        this.lastRentalDate = LocalDate.of(1, 1, 1);
        return this;
    }

    public BookBilder withTodaysRentalDate() {
        this.lastRentalDate = LocalDate.now();
        return this;
    }

    public BookBilder withLastRentalPerson(String lastRentalPerson){
        this.lastRentalPerson = lastRentalPerson;
        return this;
    }

    public BookBilder withNoLastRentalPerson() {
        this.lastRentalPerson = "none";
        return this;
    }

    public BookBilder withCopiedTitleAuthorAndIsbn(Book book){
        this.title = book.getTitle();
        this.author = book.getAuthor();
        this.isbn = book.getIsbn();
        return this;
    }

    public Book build(){
        return new Book(title, author, isbn, lastRentalDate, lastRentalPerson);
    }
}
