package book;

import file.FileService;

import java.util.List;


public class BookRepository {

    private final FileService fileService = FileService.getInstance();

    public List<Book> getListOfAllBooks(){
        return fileService.getListOfBooksFromFile();
    }

    public void saveBook(Book book){
        List<Book> bookList = getListOfAllBooks();
        bookList.add(book);
        fileService.updateFile(bookList);
    }

    public void deleteBook(String isbn){
        List<Book> bookList = getListOfAllBooks();
        bookList.removeIf(b -> b.getIsbn().equals(isbn));
        fileService.updateFile(bookList);
    }
}
