package book.service;

import book.Book;
import book.BookRepository;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class BookListsService {

    private final BookRepository bookRepository = new BookRepository();

    public List<Book> getTheListOfAllBooks() {

        return bookRepository.getListOfAllBooks();
    }

    public Map<String, Integer> getPeopleWithRentedBooks() {
        Map<String, Integer> people = new HashMap<>();
        List<Book> bookList = bookRepository.getListOfAllBooks();
        for (Book book : bookList) {
            String person = book.getLastRentalPerson();
            if (people.containsKey(person)) {
                Integer numberOfBooks = people.get(person) + 1;
                people.put(person, numberOfBooks);
            } else {
                people.put(person, 1);
            }
        }
              people.remove("none");

        return people;
    }

    public List<Book> getBooksNotRentedInXWeeks(Integer numberOfWeeks) {
        return bookRepository.getListOfAllBooks()
                .stream()
                .filter(b -> b.getLastRentalDate().isBefore(LocalDate.now().minusWeeks(numberOfWeeks)))
                .collect(Collectors.toList());
    }
}
