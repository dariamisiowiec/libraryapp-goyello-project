package book.service;

import book.Book;
import book.BookBilder;
import book.BookRepository;

public class BookService {

    private final BookBilder bookBilder = new BookBilder();
    private final BookRepository bookRepository = new BookRepository();
    private final BookFindByService bookFindByService = new BookFindByService();
    private final String ERROR_MESSAGE = "W katalogu nie ma ksiazki o numerze ISBN %s";

    public void addNewBookToCatalogue(String title, String author, String ISBN) {
        Book book = new BookBilder()
                .withTitle(title)
                .withAuthor(author)
                .withISBN(ISBN)
                .withNoLastRentalDate()
                .withNoLastRentalPerson()
                .build();
        bookRepository.saveBook(book);
    }

    public void deleteBookFromCatalogue(String isbn) {
        bookFindByService.findByIsbn(isbn)
                .orElseThrow(() -> new RuntimeException(String.format(ERROR_MESSAGE, isbn)));
        bookRepository.deleteBook(isbn);
    }

    public void rentBook(String isbn, String person) {
        Book book = bookFindByService.findByIsbn(isbn)
                .orElseThrow(() -> new RuntimeException(String.format(ERROR_MESSAGE, isbn)));
        book = bookBilder
                .withCopiedTitleAuthorAndIsbn(book)
                .withTodaysRentalDate()
                .withLastRentalPerson(person)
                .build();
        bookRepository.deleteBook(isbn);
        bookRepository.saveBook(book);
    }


}
