package book.service;

import book.Book;
import book.BookRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class BookFindByService {

    private final BookRepository bookRepository = new BookRepository();

    public Optional<Book> findByIsbn(String isbn){
        return bookRepository.getListOfAllBooks()
                .stream()
                .filter(b -> b.getIsbn().equals(isbn))
                .findFirst();
    }

    public Optional<Book> findByTitle(String title){
        return bookRepository.getListOfAllBooks()
                .stream()
                .filter(b -> b.getTitle().equals(title))
                .findFirst();
    }

    public List<Book> findByAuthor(String author){
        return bookRepository.getListOfAllBooks()
                .stream()
                .filter(b -> b.getAuthor().equals(author))
                .collect(Collectors.toList());
    }
}
