package book;

import java.time.LocalDate;

public class Book {

    private  String title;
    private  String author;
    private  String isbn;
    private  LocalDate lastRentalDate;
    private  String lastRentalPerson;

    public Book(){
        super();
    }

    public Book(String title, String author, String isbn, LocalDate lasRentalDate, String lastRentalPerson) {
        this.title = title;
        this.author = author;
        this.isbn = isbn;
        this.lastRentalDate = lasRentalDate;
        this.lastRentalPerson = lastRentalPerson;
    }


    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getIsbn() {
        return isbn;
    }

    public LocalDate getLastRentalDate() {
        return lastRentalDate;
    }

    public String getLastRentalPerson() {
        return lastRentalPerson;
    }

}
