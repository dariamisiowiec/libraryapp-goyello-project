package input;

public class UserInputParser {

    private final String ERROR_MESSAGE = "To nie jest liczba.";

    public Integer getInteger(String input) {
        try {
            return Integer.valueOf(input);
        } catch (NumberFormatException n) {
            throw new NumberFormatException(ERROR_MESSAGE);
        }
    }
}
