import command.CommandExecutorProvider;
import file.FileService;
import input.UserInputReader;
import printer.MainCommandListPrinter;
import printer.WelcomePrinter;

public class LibraryApp {

    private final WelcomePrinter welcomePrinter = new WelcomePrinter();
    private final MainCommandListPrinter mainCommandListPrinter = new MainCommandListPrinter();
    private final CommandExecutorProvider commandExecutorProvider = new CommandExecutorProvider();
    private final UserInputReader userInputReader = new UserInputReader();

    public void run(String filename) {

        FileService.getInstance().setFILENAME(filename);

        welcomePrinter.printWelcomeMessage();

        do {

            try {

                mainCommandListPrinter.printListOfCommands();

                String input = userInputReader.getUserInput();
                commandExecutorProvider.getCommand(input).execute();

            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

        } while (true);
    }
}
